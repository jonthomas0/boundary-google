resource "google_compute_instance" "boundary_controller" {
  count = var.instance_count

  name         = "${var.cluster_name}-controller-${count.index}"
  machine_type = "e2-medium"
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = data.google_compute_image.boundary.self_link
    }
  }

  network_interface {
    network    = var.network
    network_ip = google_compute_address.boundary[count.index].address

    access_config {
    }
  }

  metadata = {
    "user-data" = templatefile("${path.module}/cloud-init.yaml", {
      boundary_config_b64 = base64encode(join("\n", [
        templatefile("${path.module}/stanzas/kms.hcl", {
          purpose    = "root"
          project    = var.project
          region     = local.external_resources.key_ring.location
          key_ring   = local.external_resources.key_ring.name
          crypto_key = local.external_resources.root_kms_key.name
        }),
        templatefile("${path.module}/stanzas/kms.hcl", {
          purpose    = "worker-auth"
          project    = var.project
          region     = local.external_resources.key_ring.location
          key_ring   = local.external_resources.key_ring.name
          crypto_key = local.external_resources.worker_auth_kms_key.name
        }),
        templatefile("${path.module}/stanzas/listener.hcl", {
          purpose       = "api"
          address       = "0.0.0.0"
          tls_cert_file = "/etc/boundary/cert.pem"
          tls_key_file  = "/etc/boundary/key.pem"
        }),
        templatefile("${path.module}/stanzas/listener.hcl", {
          purpose       = "cluster"
          address       = google_compute_address.boundary[count.index].address
          tls_cert_file = "/etc/boundary/cert.pem"
          tls_key_file  = "/etc/boundary/key.pem"
        }),
        templatefile("${path.module}/stanzas/controller.hcl", {
          name = "${var.cluster_name}-controller-${count.index}"
          database_url = format("postgresql://%s:%s@localhost:5432/boundary?sslmode=disable",
            urlencode(local.external_resources.postgres_user.name),
            urlencode(local.external_resources.postgres_user.password)
          )
        }),
      ]))
      tls_key_b64 = base64encode(tls_private_key.boundary[count.index].private_key_pem)
      tls_crt_b64 = base64encode(join("\n", [
        tls_locally_signed_cert.boundary[count.index].cert_pem,
        data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem,
        data.terraform_remote_state.ca.outputs.root_ca_certificate_pem,
      ]))
      cloud_sql_proxy_service_b64 = base64encode(templatefile("${path.module}/systemd/cloud-sql-proxy.service", {
        connection_name = local.external_resources.postgres_instance.connection_name
      }))
    })
  }

  service_account {
    email = google_service_account.controller.email

    scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}
