output "postgres_instance" {
  value = local.external_resources.postgres_instance
}

output "postgres_user" {
  value = local.external_resources.postgres_user
}
