data "terraform_remote_state" "external_resources" {
  backend = "gcs"

  config = {
    bucket = "adrienne-devops-terraform-states"
    prefix = "gitlab.com/adriennes-spells/boundary-google-external-resources"
  }
}

locals {
  external_resources = data.terraform_remote_state.external_resources.outputs
}
