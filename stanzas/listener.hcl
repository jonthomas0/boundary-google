listener "tcp" {
  purpose       = "${purpose}"
  address       = "${address}"
  tls_cert_file = "${tls_cert_file}"
  tls_key_file  = "${tls_key_file}"
}
