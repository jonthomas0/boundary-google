resource "google_compute_forwarding_rule" "boundary" {
  name            = var.cluster_name
  region          = var.region
  backend_service = google_compute_region_backend_service.boundary.id

  ip_address = google_compute_address.boundary_loadbalancer.address
  ports = [9200]
}

resource "google_compute_region_backend_service" "boundary" {
  name   = var.cluster_name
  region = var.region

  load_balancing_scheme = "EXTERNAL"

  health_checks = [google_compute_region_health_check.boundary.self_link]

  backend {
    group = google_compute_instance_group.boundary.self_link

    balancing_mode = "CONNECTION"
  }
}

resource "google_compute_instance_group" "boundary" {
  name      = var.cluster_name
  zone      = var.zone
  network   = data.google_compute_network.boundary.id
  instances = google_compute_instance.boundary_controller.*.self_link
}

resource "google_compute_region_health_check" "boundary" {
  name   = var.cluster_name
  region = var.region

  timeout_sec        = 2
  check_interval_sec = 5

  https_health_check {
    port = 9200
  }
}

data "google_compute_network" "boundary" {
  name = var.network
}

data "google_compute_subnetwork" "boundary" {
  name   = var.subnetwork
  region = var.region
}
