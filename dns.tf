resource "google_dns_record_set" "boundary" {
  name = "boundary.${data.google_dns_managed_zone.personal.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = data.google_dns_managed_zone.personal.name

  rrdatas = [google_compute_address.boundary_loadbalancer.address]
}

resource "google_compute_address" "boundary_loadbalancer" {
  name = var.cluster_name
  region = var.region
}

data "google_dns_managed_zone" "personal" {
  name = var.dns_managed_zone
}
